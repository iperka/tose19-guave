package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle User. Hier werden alle Funktionen für die DB-Operationen
 * zu User implementiert
 * 
 * @author Guave
 *
 */

public class UserRepository {

	private final Logger log = LoggerFactory.getLogger(UserRepository.class);

	/**
	 * Liefert alle User in der Datenbank
	 * 
	 * @return Collection aller User
	 */
	public Collection<User> getAll() {
		log.trace("getAll");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select userid, name from users");
			ResultSet rs = stmt.executeQuery();
			return mapUsers(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all Users. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert den User mit der übergebenen userId
	 * 
	 * @param iserId id des Users
	 * @return User oder NULL
	 */
	public User getByUserId(long userId) {
		log.trace("getByUserId " + userId);

		// TODO: There is an issue with this repository method. Find and fix it!
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select userid, name from users where userid=?");
			stmt.setLong(1, userId);
			ResultSet rs = stmt.executeQuery();
			return mapUsers(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving users by userId " + userId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert den übergebenen user in der Datenbank. UPDATE.
	 * 
	 * @param i
	 */
	public void save(User i) {
		log.trace("save " + i);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("update users set name=? where userid=?");
			stmt.setString(1, i.getName());
			stmt.setLong(2, i.getUserId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while saving user " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Löscht den User, mit der übergebenen UserId.
	 */
	public void delete(long userId) {
		log.trace("delete User " + userId);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from users where userid=?");
			stmt.setLong(1, userId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while deleteing User " + userId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	public long insert(User i) {

		log.trace("insert " + i);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("insert into users (name) values (?)");
			stmt.setString(1, i.getName());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		} catch (SQLException e) {
			String msg = "SQL error while inserting user " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * assoziiert zuerst die aktuellen Übungen mit dem User und speichert diese dann
	 * in den Table "savedItems". Anschliessend wird die Assoziation des Items
	 * wieder gelöscht. So bleibt der ursprüngliche Plan von der Sicherung quasi
	 * unbetroffen.
	 */
	public void associate(long userId) {
		log.trace("save plan with current items under user " + userId);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("update items set userid=?");
			stmt.setLong(1, userId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while associating current items with user " + userId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("insert into saveditems select * from items where userid=?");
			stmt.setLong(1, userId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while inserting current items to saveditems for user  " + userId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("update items set userid=null");
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while resetting current items after saving them under User  " + userId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Helper zum konvertieren der Resultsets in user-Objekte. Siehe getByXXX
	 * Methoden.
	 * 
	 * @throws SQLException
	 *
	 */
	private static Collection<User> mapUsers(ResultSet rs) throws SQLException {
		LinkedList<User> list = new LinkedList<User>();
		while (rs.next()) {
			User i = new User(rs.getLong("userId"), rs.getString("name"));
			list.add(i);
		}
		return list;
	}

}
