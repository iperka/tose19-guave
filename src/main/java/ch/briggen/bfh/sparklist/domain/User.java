package ch.briggen.bfh.sparklist.domain;

/**
 * Userm der einen Plan unter seinem Namen speichern kann.
 * 
 * @author Marcel Briggen
 *
 */
public class User {

	private long userId;
	private String name;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public User() {

	}

	/**
	 * Konstruktor
	 * 
	 * @param userId Eindeutige Id des Users
	 * @param name   Name des Users
	 * 
	 */
	public User(long userId, String name) {
		this.userId = userId;
		this.name = name;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return String.format("User:{userId: %d; name: %s;}", userId, name);

	}

}
