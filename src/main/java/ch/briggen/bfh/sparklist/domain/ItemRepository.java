package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle Items. Hier werden alle Funktionen für die DB-Operationen
 * zu Items implementiert
 * 
 * @author Marcel Briggen
 *
 */

public class ItemRepository {

	private final Logger log = LoggerFactory.getLogger(ItemRepository.class);

	/**
	 * Liefert alle items in der Datenbank
	 * 
	 * @return Collection aller Items
	 */
	public Collection<Item> getAll() {
		log.trace("getAll");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select id, exercise, reps, sets, difficulty from items");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Items der angegebenen Übung
	 * 
	 * @param exercise
	 * @return Collection mit dem Namen "exercise"
	 */
	public Collection<Item> getByExercise(String exercise) {
		log.trace("getByExercise " + exercise);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select id, exercise, reps, sets, difficulty from items where exercise=?");
			stmt.setString(1, exercise);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving items by exercise " + exercise;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Liefert das Item mit der übergebenen Id
	 * 
	 * @param id id des Item
	 * @return Item oder NULL
	 */
	public Item getById(long id) {
		log.trace("getById " + id);

		// TODO: There is an issue with this repository method. Find and fix it!
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select id, exercise, reps, sets, difficulty from items where id=?");
			stmt.setInt(1, (int) id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert das übergebene item in der Datenbank. UPDATE.
	 * 
	 * @param i
	 */
	public void save(Item i) {
		log.trace("save " + i);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("update items set exercise=?, reps=?, sets=?, difficulty=? where id=?");
			stmt.setString(1, i.getExercise());
			stmt.setInt(2, i.getReps());
			stmt.setInt(3, i.getSets());
			stmt.setInt(4, i.getSets());
			stmt.setInt(4, (int) i.getDifficulty());
			stmt.setLong(5, i.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while updating item " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * ladet die Items aus savedItems, die mit dem User der sie lädt, assoziiert
	 * sind.
	 */

	public Collection<Item> load(long userId) {
		log.trace("load plan associated with user " + userId);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select * from saveditems where userid=?");
			stmt.setLong(1, userId);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);
		} catch (SQLException e) {
			String msg = "SQL error while loading items associated with user " + userId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Löscht alle Items aus Saveditems, die mit der UserId assoziiert sind.
	 */
	public void deleteSavedItems(long userId) {
		log.trace("delete saved items associated with User " + userId);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from saveditems where userid=?");
			stmt.setLong(1, userId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while deleteing saved items associated with User " + userId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Löscht das Item mit der angegebenen Id von der DB
	 * 
	 * @param id Item ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from items where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while deleteing items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	public long insert(Item i) {

		log.trace("insert " + i);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("insert into items (exercise, reps, sets, difficulty) values (?,?,?,?)");
			stmt.setString(1, i.getExercise());
			stmt.setInt(2, i.getReps());
			stmt.setInt(3, i.getSets());
			stmt.setInt(4, (int) i.getDifficulty());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		} catch (SQLException e) {
			String msg = "SQL error while updating item " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Die folgenden Methoden laden jeweils x (x=Berechnete Anzahl der Übungen)
	 * Items eines Levels oder cardio.
	 */

	public Collection<Item> getNoobExercises() {
		log.trace("get noob exercises");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select id, exercise, reps, sets, difficulty from pool where difficulty=1 limit ?");
			stmt.setInt(1, (int) Tailoring.noobExercises);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving noob exercises";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	public Collection<Item> getIntermediateExercises() {
		log.trace("get intermediate exercises");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select id, exercise, reps, sets, difficulty from pool where difficulty=2 limit ?");
			stmt.setInt(1, (int) Tailoring.intermediateExercises);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving intermediate exercises";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	public Collection<Item> getProExercises() {
		log.trace("get pro exercises");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select id, exercise, reps, sets, difficulty from pool where difficulty=3 limit ?");
			stmt.setInt(1, (int) Tailoring.proExercises);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving pro exercises";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	public Collection<Item> getCardioExercises() {
		log.trace("get cardio exercises");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select id, exercise, reps, sets, difficulty from pool where isCardio=1 limit ?");
			stmt.setInt(1, (int) Tailoring.nrOfCardio);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving cardio exercises";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Helper zum konvertieren der Resultsets in Item-Objekte. Siehe getByXXX
	 * Methoden.
	 * 
	 * @author Marcel Briggen
	 * @throws SQLException
	 *
	 */
	private static Collection<Item> mapItems(ResultSet rs) throws SQLException {
		LinkedList<Item> list = new LinkedList<Item>();
		while (rs.next()) {
			Item i = new Item(rs.getLong("id"), rs.getString("exercise"), rs.getInt("reps"), rs.getInt("sets"),
					rs.getInt("difficulty"));
			list.add(i);
		}
		return list;
	}

}
