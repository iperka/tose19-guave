package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * 
 * @author Marcel Briggen
 *
 */
public class Item {

	private long id;
	private String exercise;
	private int reps;
	private int sets;
	private int difficulty;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Item() {

	}

	/**
	 * Konstruktor
	 * 
	 * @param id         Eindeutige Id
	 * @param exercise   Name der Übung
	 * @param reps       Anzahl Repetitionen pro Set
	 * @param sets       Anzahl durchgeführter Sets
	 * @param difficulty Schwierigkeit der Übung 1-3
	 * 
	 */
	public Item(long id, String exercise, int reps, int sets, int difficulty) {
		this.id = id;
		this.exercise = exercise;
		this.reps = reps;
		this.sets = sets;
		this.difficulty = difficulty;
	}

	public String getExercise() {
		return exercise;
	}

	public void setExercise(String exercise) {
		this.exercise = exercise;
	}

	public int getReps() {
		return reps;
	}

	public void setReps(int reps) {
		this.reps = reps;
	}

	public int getSets() {
		return sets;
	}

	public void setSets(int sets) {
		this.sets = sets;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDifficulty() {
		return difficulty;
	}

	@Override
	public String toString() {
		return String.format("Item:{id: %d; exercise: %s; reps: %d; sets: %d, difficulty: %d}", id, exercise, reps,
				sets, difficulty);

	}

}
