package ch.briggen.bfh.sparklist.domain;

/**
 * Klasse zur Angabe von persönlichen Daten und die Verwendung dieser zur
 * Berechnung eines Plans
 * 
 * @author Guave
 *
 */
public class Tailoring {

	private static int gender;
	private static int weight;
	private static int age;
	private static int goal;
	private static int level;
	private static int time;

	static double nrOfExercises = 0;
	static double nrOfCardio = 0;
	public static double noobExercises = 0;
	public static double intermediateExercises = 0;
	public static double proExercises = 0;

	/**
	 * Konstruktor
	 * 
	 * @param gender 1=male 2=female 3=other
	 * @param weight gewicht in kg
	 * @param age    alter in Altersgruppe ( 15 = 18-25 years 12 = 26-30 years 10 =
	 *               31-35 years 9 = 36-40 years 8 = 41-50 years 7 = 51-55 years 6 =
	 *               56-60 years 5 = 65-70 years )
	 * 
	 * @param goal   1=gain muscle 2=lose weight
	 * 
	 * @param level  1=noob 2=intermediate 3=pro
	 * 
	 * @param time   zeit in minuten
	 */
	public Tailoring(int gender, int weight, int age, int goal, int level, int time) {
		Tailoring.gender = gender;
		Tailoring.weight = weight;
		Tailoring.age = age;
		Tailoring.goal = goal;
		Tailoring.level = level;
		Tailoring.time = time;

	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		Tailoring.gender = gender;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		Tailoring.weight = weight;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		Tailoring.age = age;
	}

	public long getGoal() {
		return goal;
	}

	public void setGoal(int goal) {
		Tailoring.goal = goal;
	}

	public long getLevel() {
		return level;
	}

	public void setLevel(int level) {
		Tailoring.level = level;
	}

	public long getTime() {
		return time;
	}

	public void setTime(int time) {
		Tailoring.time = time;
	}

	@Override
	public String toString() {
		return String.format("Tailored:{gender: %d; weight: %d; height: %d; goal: %d; level: %d; time: %d}", gender,
				weight, age, goal, level, time);
	}

	// Berechnet wieviele Übungen der User erhält und wieviele davon Ausdauerübungen
	// sind.
	public static void calculateExercises(Tailoring tailoredDetail) {

		// Je höher das Level des Users und je mehr Zeit er hat, desto mehr Übungen
		nrOfExercises = Math.round((level * 0.5));
		nrOfExercises = Math.round(nrOfExercises * (time * 0.1));

		// Hat der User angegeben, er will abnehmen (goal==2), wird ein drittel
		// (gerundet) der vorher berechneten Übungen dafür reserviert.
		if (goal == 2) {
			nrOfCardio = Math.round(nrOfExercises / 3);
			nrOfExercises = nrOfExercises - nrOfCardio;
		}

		// Je nach Level des Users wird der Anteil an einfachen, mittleren und
		// schwierigen Übungen festgelegt.
		if (level == 1) {
			noobExercises = nrOfExercises;
			intermediateExercises = 0;
			proExercises = 0;
		} else if (level == 2) {
			noobExercises = Math.round(nrOfExercises * 0.5);
			intermediateExercises = Math.round(nrOfExercises - noobExercises);
			proExercises = 0;
		} else if (level == 3) {
			noobExercises = Math.round(nrOfExercises * 0.25);
			intermediateExercises = Math.round(nrOfExercises * 0.25);
			proExercises = Math.round(nrOfExercises - noobExercises - intermediateExercises);
		}

	}

	public static double calculateCalories() {

		// Total Calories Burned = tcb
		double tcb;
		// Altersgruppe hat Einfluss auf Kalorienverbrennung
		double ageFactor = age * 0.1;

		// Formel zur Berechnung der verbrannten Kalorien
		tcb = ((time * (5 * 3.5 * weight)) / 200) * ageFactor;

		// Wenn der User Cardio Übungen hat verbrennt er für einen Drittel der Übungen
		// zusätzlich Kalorien
		if (nrOfCardio > 0) {
			tcb = (((time / 3 * 2) * (5 * 3.5 * weight)) / 200) * ageFactor
					+ (((time / 3) * (8 * 3.5 * weight)) / 200) * ageFactor;
		}

		// Erfahrene User verbrennen tendenziell weniger Kalorien
		if (level == 3) {
			tcb = tcb * 0.8;
		} else if (level == 2) {
			tcb = tcb * 0.9;
		}

		// Männer verbrennen tendenziell 20% mehr Kalorien
		if (gender == 1) {
			tcb = tcb * 1.2;
		}

		return Math.round(tcb);

	}

}
