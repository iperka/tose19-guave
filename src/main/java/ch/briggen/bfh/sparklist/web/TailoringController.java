package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class TailoringController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(TailoringController.class);

	ItemRepository repository = new ItemRepository();

	/**
	 * Controller für das tailoring template. User kann Formular ausfüllen.
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		HashMap<String, Collection<Item>> model = new HashMap<String, Collection<Item>>();

		return new ModelAndView(model, "tailoringTemplate");

	}
}
