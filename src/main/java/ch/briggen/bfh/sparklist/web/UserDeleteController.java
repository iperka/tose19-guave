package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für löschen eines gesicherten Plans und dessen User.
 * 
 * @author Marcel Briggen
 *
 */

public class UserDeleteController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(UserDeleteController.class);

	private ItemRepository itemRepo = new ItemRepository();
	private UserRepository userRepo = new UserRepository();

	/**
	 * Löscht alle Items aus SavedItems, die mit der UserId assoziiert sind. Löscht
	 * den User mit der übergebenen userId.
	 * 
	 * Hört auf GET /yourLoadedPlan/delete
	 * 
	 * @return Redirect zurück zur Liste
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String userId = request.queryParams("userId");
		log.trace("GET /yourLoadedPlan/delete mit userId " + userId);

		Long longId = Long.parseLong(userId);
		itemRepo.deleteSavedItems(longId);
		userRepo.delete(longId);

		response.redirect("/yourPlan");
		return null;
	}
}
