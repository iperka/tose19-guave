package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller Liefert unter "/" die Index Seite
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung
 * !!!
 * 
 * @author M. Briggen
 *
 */
public class IndexController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(IndexController.class);

	ItemRepository repository = new ItemRepository();

	/**
	 * Liefert die Liste als Index-Seite "/" zurück
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		HashMap<String, Collection<Item>> model = new HashMap<String, Collection<Item>>();

		return new ModelAndView(model, "indexTemplate");
	}
}
