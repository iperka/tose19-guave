package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Tailoring;
import spark.Request;

class TailoringWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(TailoringWebHelper.class);

	public static Tailoring tailoredFromWeb(Request request) {
		return new Tailoring(Integer.parseInt(request.queryParams("tailored.gender")),
				Integer.parseInt(request.queryParams("tailored.weight")),
				Integer.parseInt(request.queryParams("tailored.age")),
				Integer.parseInt(request.queryParams("tailored.goal")),
				Integer.parseInt(request.queryParams("tailored.level")),
				Integer.parseInt(request.queryParams("tailored.time")));
	}

}
