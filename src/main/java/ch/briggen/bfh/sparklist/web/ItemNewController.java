package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Items !!! Diese Version verfügt
 * bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Marcel Briggen
 *
 */

public class ItemNewController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(ItemNewController.class);

	private ItemRepository itemRepo = new ItemRepository();

	/**
	 * Erstellt ein neues Item in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Plan Seite redirected
	 * 
	 * Hört auf POST /item/new
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		Item itemDetail = ItemWebHelper.itemFromWeb(request);
		log.trace("POST /item/new mit itemDetail " + itemDetail);

		// insert gibt die von der DB erstellte id zurück.

		itemRepo.insert(itemDetail);

		// der redirect erfolgt dann auf /yourPlan
		response.redirect("/yourPlan");
		return null;
	}
}
