package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller Liefert unter "/load" die liste aller User, damit ein Plan geladen
 * werden kann.
 */
public class UserLoadController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(UserLoadController.class);

	UserRepository repository = new UserRepository();

	/**
	 * Liefert die Liste der User der Seite "/load"
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		// User werden geladen und die Collection dann für das Template unter dem namen
		// "userList" bereitgestellt
		// Das Template muss dann auch den Namen "userList" verwenden.
		HashMap<String, Collection<User>> model = new HashMap<String, Collection<User>>();
		model.put("userList", repository.getAll());
		return new ModelAndView(model, "userLoadTemplate");
	}
}
