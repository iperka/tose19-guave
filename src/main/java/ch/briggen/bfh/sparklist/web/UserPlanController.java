package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller zur Anzeige des vom user geladenen Plans
 */

public class UserPlanController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(UserPlanController.class);
	private ItemRepository itemRepo = new ItemRepository();
	private UserRepository userRepo = new UserRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		long userId = UserLoadWebHelper.userFromWeb(request);

		log.trace("POST /user für user mit id " + userId);

		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("userPlan", itemRepo.load(userId));
		model.put("userName", userRepo.getByUserId(userId));
		return new ModelAndView(model, "userPlanTemplate");
	}
}
