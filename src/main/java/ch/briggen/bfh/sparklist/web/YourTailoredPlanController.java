package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Tailoring;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class YourTailoredPlanController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(YourTailoredPlanController.class);

	ItemRepository repository = new ItemRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		// Die zuvor berechneten Übungen werden geladen und dem Template unter den Namen
		// "tailoredNoob" (-Intermediate, -Pro, etc.) bereitgestellt.
		// Das Template muss dann auch diese Namen verwenden.

		HashMap<String, Object> model = new HashMap<String, Object>();

		model.put("tailoredNoob", repository.getNoobExercises());
		model.put("tailoredIntermediate", repository.getIntermediateExercises());
		model.put("tailoredPro", repository.getProExercises());
		model.put("tailoredCardio", repository.getCardioExercises());
		model.put("tailoredCalories", Tailoring.calculateCalories());

		return new ModelAndView(model, "yourTailoredPlanTemplate");

	}

}
