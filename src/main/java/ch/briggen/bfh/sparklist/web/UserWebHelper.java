package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.User;
import spark.Request;

class UserWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(UserWebHelper.class);

	public static User userFromWeb(Request request) {
		return new User(Long.parseLong(request.queryParams("userDetail.userId")),
				request.queryParams("userDetail.name"));
	}

}
