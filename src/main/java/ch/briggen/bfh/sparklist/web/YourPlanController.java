package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class YourPlanController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(YourPlanController.class);

	ItemRepository repository = new ItemRepository();
	UserRepository userRepository = new UserRepository();

	/**
	 * Liefert die Liste als Root-Seite "/" zurück
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		// Items werden geladen und die Collection dann für das Template unter dem namen
		// "yourPlan" bereitgestellt
		// Das Template muss dann auch den Namen "yourPlan" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("yourPlanExercises", repository.getAll());
		model.put("yourPlanUsers", userRepository.getAll());

		return new ModelAndView(model, "yourPlanTemplate");

	}
}
