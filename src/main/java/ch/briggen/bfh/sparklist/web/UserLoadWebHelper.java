package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.Request;

class UserLoadWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(UserLoadWebHelper.class);

	public static long userFromWeb(Request request) {
		return Long.parseLong(request.queryParams("userDetail.userId"));
	}

}
