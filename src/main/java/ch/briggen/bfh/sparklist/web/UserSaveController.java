package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für die Speicherung eines Plans auf einen User
 */

public class UserSaveController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(UserSaveController.class);

	private UserRepository userRepo = new UserRepository();

	/**
	 * Erstellt ein neuer User in der DB. Die userId wird von der Datenbank
	 * erstellt. Bei Erfolg wird wieder auf die Detailseite redirected (z.B.:
	 * /user&id=99 wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/new
	 * 
	 * @return Redirect zurück zur yourPlan Maske
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		User userDetail = UserWebHelper.userFromWeb(request);
		log.trace("POST /user/new mit userDetail " + userDetail);

		// insert gibt die von der DB erstellte userId zurück.
		Long userId = userRepo.insert(userDetail);

		// assoziieren der neu generierten ID mit aktuellen items
		userRepo.associate(userId);

		response.redirect("/yourPlan");
		return null;
	}
}
