package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.User;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für Operation auf User
 */

public class UserEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(UserEditController.class);

	/**
	 * Requesthandler zum Bearbeiten eines Users. Liefert das Formular (bzw.
	 * Template) zum eingeben des Namens. Nach Eingabe wird beim submitten des
	 * Formulars der Plan unter dem User gesichert (Aufruf /user/new) Hört auf GET
	 * /user
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer
	 *         "UserDetailTemplate" .
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("userId");
		HashMap<String, Object> model = new HashMap<String, Object>();

		log.trace("GET /user für INSERT mit userId " + idString);
		// der Submit-Button ruft /user/new auf --> INSERT
		model.put("postAction", "/user/new");
		model.put("userDetail", new User());

		// das Template userDetail verwenden
		return new ModelAndView(model, "userDetailTemplate");
	}

}
