package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Tailoring;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller zur Berechnung des Plans basierend auf Angaben des Users.
 */

public class TailoringCalculateController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(TailoringCalculateController.class);
	int x = (int) Tailoring.noobExercises;

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		/*
		 * Die Angaben des Users werden aus dem Request geholt und verarbeitet. Die
		 * Übungen und die verbrannten Kalorien werden berechnet.
		 */

		Tailoring tailoredDetail = TailoringWebHelper.tailoredFromWeb(request);
		log.trace("POST /tailored mit tailoredDetail. " + tailoredDetail);

		Tailoring.calculateExercises(tailoredDetail);
		log.trace("Plan is being tailored to users data");

		double totalCaloriesBurned = Tailoring.calculateCalories();
		log.trace("User burns about :" + totalCaloriesBurned + " calories total.");

		// Nach Berechnung wird User zum fertigen Plan weitergeleitet.
		response.redirect("/yourTailoredPlan");
		return null;
	}
}
