package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ItemTest {

	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Item i = new Item();
		assertThat("Id", i.getId(), is(equalTo(0l)));
		assertThat("Exercise", i.getExercise(), is(equalTo(null)));
		assertThat("Reps", i.getReps(), is(equalTo(0)));
		assertThat("Sets", i.getSets(), is(equalTo(0)));
	}

	@ParameterizedTest
	@CsvSource({ "1,One,-1", "2,Two,0", "3,Three,1" })
	void testContructorAssignsAllFields(long id, String exercise, int reps, int sets, int difficulty) {
		Item i = new Item(id, exercise, reps, sets, difficulty);
		assertThat("Id", i.getId(), equalTo(id));
		assertThat("Exercise", i.getExercise(), equalTo(exercise));
		assertThat("Reps", i.getReps(), equalTo(reps));
		assertThat("Sets", i.getSets(), equalTo(sets));
	}

	@ParameterizedTest
	@CsvSource({ "1,One,-1", "2,Two,0", "3,Three,1" })
	void testSetters(long id, String exercise, int reps, int sets) {
		Item i = new Item();
		i.setId(id);
		i.setExercise(exercise);
		i.setReps(reps);
		i.setSets(sets);
		assertThat("Id", i.getId(), equalTo(id));
		assertThat("exercise", i.getExercise(), equalTo(exercise));
		assertThat("reps", i.getReps(), equalTo(reps));
		assertThat("sets", i.getSets(), equalTo(sets));
	}

	@Test
	void testToString() {
		Item i = new Item();
		System.out.println(i.toString());
		assertThat("toString smoke test", i.toString(), not(nullValue()));
	}
}
